import {
  Component,
  Input,
  OnInit,
  Output,
  SimpleChanges,
  EventEmitter,
} from '@angular/core'
import { PokemonApiService } from 'src/app/services/pokemon-api-service/pokemon-api.service'

@Component({
  selector: 'app-pokemon-print',
  templateUrl: './pokemon-print.component.html',
  styleUrls: ['./pokemon-print.component.scss'],
})
export class PokemonPrintComponent implements OnInit {
  @Input() public pokemonToSon: object
  @Input() public pokemonName: string | null = null
  @Input() public pokemonId: number | null = null
  public pokemonInfo: object

  public pokemonList: Array<object> = []

  @Output() public emitPokemon = new EventEmitter<object>()
  constructor(private pokemonApiService: PokemonApiService) {}

  ngOnInit(): void {}
  ngOnChanges(changes: SimpleChanges): void {
    this.getPokemonByName()
    this.getRandomPokemon()
  }
  public addPokemonToList() {
    this.pokemonList.length <= 5 && this.pokemonToSon
      ? this.pokemonList.push(this.pokemonToSon)
      : console.log('You reached your limit')
    console.log(this.pokemonList.length)
    // if (this.pokemonToSon && this.pokemonList.length <= 5) {

    // }
  }
  public getPokemonByName() {
    if (this.pokemonName !== undefined && this.pokemonName !== null) {
      this.pokemonApiService
        .getPokemonByName(this.pokemonName)
        .subscribe((data: any) => {
          const formatedResult = {
            name: data.name,
            height: data.height,
            weight: data.weight,
            img: data.sprites.other.dream_world.front_default,
          }
          this.pokemonInfo = formatedResult
        })
      return this.pokemonInfo
    }
  }
  public getRandomPokemon() {
    if (this.pokemonId !== undefined && this.pokemonId !== null) {
      this.pokemonApiService
        .getRandomPokemon(this.pokemonId)
        .subscribe((data: any) => {
          const formatedResult = {
            name: data.name,
            height: data.height,
            weight: data.weight,
            img: data.sprites.other.dream_world.front_default,
          }
          this.pokemonInfo = formatedResult
        })
      return this.pokemonInfo
    }
  }
  sendPokemon() {
    this.emitPokemon.emit(this.pokemonInfo)
  }
}
