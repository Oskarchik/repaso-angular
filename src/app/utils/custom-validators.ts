import { FormGroup } from '@angular/forms'

// export function compareFields(
//   controlField: string,
//   matchingControlField: string,
// ) {
//   return (formGroup: FormGroup) => {
//     const control = formGroup.controls[controlField]
//     const matchingControl = formGroup.controls[matchingControlField]
//     if (matchingControl.errors && !matchingControl.errors.mustMatch) {
//       return
//     }
//     if (control.value !== matchingControl.value) {
//       matchingControl.setErrors({ mustMatch: true })
//     } else {
//       matchingControl.setErrors(null)
//     }
//   }
// }
export function compareFields(
  controlName: string,
  matchingControlName: string,
) {
  return (formGroup: FormGroup) => {
    // Asignamos dos controladores a nuestros valores por param
    const control = formGroup.controls[controlName]
    const matchingControl = formGroup.controls[matchingControlName]
    //  Control de errores
    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      return
    }
    // Setter Errores
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true })
    } else {
      matchingControl.setErrors(null)
    }
  }
}
