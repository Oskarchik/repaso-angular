import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { HeaderRoutingModule } from './header-routing.module'
import { HeaderComponent } from './header-component/header.component'
import { HomeComponent } from '../../pages/home/home.component'

@NgModule({
  declarations: [HeaderComponent, HomeComponent],
  imports: [CommonModule, HeaderRoutingModule],
})
export class HeaderModule {}
