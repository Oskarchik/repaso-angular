export interface Registration {
  name: string
  nickname: string
  email: string
  confirmEmail: string
  password: string
  confirmPassword: string
}
