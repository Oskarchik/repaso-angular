import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { ThrowStmt } from '@angular/compiler'

@Injectable({
  providedIn: 'root',
})
export class PokemonApiService {
  public baseUrl: string = 'https://pokeapi.co/api/v2/pokemon/'
  public pokemonName: string = ''
  public pokemonId: number | null = null
  constructor(private http: HttpClient) {}

  public getPokemonList(id: number) {
    return this.http.get(this.baseUrl + id)
  }
  public getPokemonByName(name: string) {
    if (name === undefined) {
      return
    }
    this.pokemonName = name
    return this.http.get(this.baseUrl + this.pokemonName)
  }
  public getRandomPokemon(id: number) {
    if (id === undefined) {
      return
    }
    this.pokemonId = id
    return this.http.get(this.baseUrl + this.pokemonId)
  }
}
