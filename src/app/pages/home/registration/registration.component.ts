import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { Registration } from './models/registration'
import { compareFields } from '../../../utils/custom-validators'

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent implements OnInit {
  public registrationForm: FormGroup
  public isSubmitted: boolean = false
  public formCompleted: Registration | null = null
  constructor(private formBuilder: FormBuilder) {
    this.registrationForm = this.formBuilder.group(
      {
        name: [
          '',
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(20),
          ],
        ],
        nickname: [
          '',
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(15),
          ],
        ],
        email: ['', [Validators.required, Validators.email]],
        confirmEmail: ['', Validators.required, Validators.email],
        password: [
          '',
          Validators.required,
          Validators.minLength(6),
          Validators.pattern(
            /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,10}$/,
          ),
        ],
        confirmPassword: [
          '',
          Validators.required,
          Validators.minLength(6),
          Validators.pattern(
            /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,10}$/,
          ),
        ],
      },
      {
        validatorEmail: compareFields('email', 'confirmEmail'),
        validatorPassword: compareFields('password', 'confirmPassword'),
      },
    )
  }

  ngOnInit(): void {}

  public onSubmit(): void {
    this.isSubmitted = true

    if (this.registrationForm?.valid) {
      const REGISTRATION = {
        name: this.registrationForm.get('name')?.value,
        nickname: this.registrationForm.get('nickname')?.value,
        email: this.registrationForm.get('email')?.value,
        confirmEmail: this.registrationForm.get('confirmEmail')?.value,
        password: this.registrationForm.get('password')?.value,
        confirmPassword: this.registrationForm.get('confirmPassword')?.value,
      }
      console.log(REGISTRATION)
      this.formCompleted = REGISTRATION
      this.registrationForm.reset()
      this.isSubmitted = false
    }
  }
}
