import { Component, OnInit } from '@angular/core'
import { MovieApiService } from 'src/app/services/movie-api-service/movie-api.service'
import { PokemonApiService } from 'src/app/services/pokemon-api-service/pokemon-api.service'

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  public pokemonInfo: any
  public pokemonToSon: any
  public pokemonName: string
  public pokemonId: number
  public receivedPokemon: object
  public receivedPokemonList: Array<object> = []
  public received: boolean = false
  public limitError: string = ''
  //public movieSearched: any
  constructor(
    public pokemonApiService: PokemonApiService,
    public movieApiService: MovieApiService,
  ) {}

  ngOnInit(): void {}

  public sendPokeName(name: string) {
    this.pokemonName = name
  }
  public randomId() {
    this.pokemonId = Math.floor(Math.random() * 1000) + 1
    console.log(this.pokemonId)
  }
  public setPokemon(pokemon: object): void {
    this.receivedPokemon = pokemon
    this.receivedPokemonList.length < 5
      ? this.receivedPokemonList.push(this.receivedPokemon)
      : this.setLimitError()
    this.received = true
  }
  public setLimitError() {
    return (this.limitError = 'Has alcanzado tu límite')
  }
}
