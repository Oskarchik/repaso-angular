import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { SearchRoutingModule } from './search-routing.module'
import { SearchComponent } from './search-component/search.component';
import { PokemonPrintComponent } from './search-component/pokemon-print/pokemon-print.component'

@NgModule({
  declarations: [SearchComponent, PokemonPrintComponent],
  imports: [CommonModule, SearchRoutingModule],
})
export class SearchModule {}
