import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'

@Injectable({
  providedIn: 'root',
})
export class MovieApiService {
  public baseEndPoint: string = 'http://www.omdbapi.com/?t='
  public apiKey: string = '&apikey=5b8043ac'
  public query: string

  constructor(private http: HttpClient) {}

  public getSearchedMovie(title: string) {
    this.query = title.replace(/\s/g, '+')
    return this.http.get(this.baseEndPoint + this.query + this.apiKey)
  }
}
