import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonPrintComponent } from './pokemon-print.component';

describe('PokemonPrintComponent', () => {
  let component: PokemonPrintComponent;
  let fixture: ComponentFixture<PokemonPrintComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokemonPrintComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokemonPrintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
